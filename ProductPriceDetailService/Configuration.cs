﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPiServer.ExpertServices.ProductLevelPriceProvider
{
    /// <summary>
    /// Configuration class to access the Product Level Pricing application settings key.
    /// A true value means to store the price on the product while a false means to synchronize the price among the variants instead.
    /// </summary>
    public static class Configuration
    {
        private const string ProductLevelPricingName = "ProductLevelPricing";
        public static readonly Lazy<String> ProductLevelConfiguration = new Lazy<string>(() => ConfigurationManager.AppSettings[ProductLevelPricingName]);
    }
}
