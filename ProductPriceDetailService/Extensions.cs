﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using EPiServer.Commerce.Catalog.Linking;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Mediachase.Commerce.Catalog;
using Mediachase.Commerce.Catalog.Managers;
using Mediachase.Commerce.Catalog.Objects;
using EPiServer.Commerce.Extensions;
using EPiServer.Core.Html.StringParsing;
using ICSharpCode.SharpZipLib.Zip;
using Mediachase.Commerce.Pricing;
using EPiServer;
using EPiServer.Commerce.Catalog.ContentTypes;
using Mediachase.Commerce.Catalog.Dto;

namespace EPiServer.ExpertServices.ProductLevelPriceProvider
{
    public static class Extensions
    {
        private static readonly ServiceLocationHelper Locate = new ServiceLocationHelper(ServiceLocator.Current);
        private static readonly IContentRepository ContentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();

        /// <summary>
        /// Find the first parent of the variant
        /// </summary>
        /// <param name="variantContentReference"></param>
        /// <returns></returns>
        public static ProductContent GetFirstProductParent(this ContentReference variantContentReference)
        {
            // TODO: Add caching for getting the first product parent.

            ContentReference productReference = variantContentReference.FirstProduct();
            return productReference != null ? ContentRepository.Get<ProductContent>(productReference) : null;
        }

        /// <summary>
        /// Find the first parent of the variant
        /// </summary>
        /// <param name="variantContentReference"></param>
        /// <returns></returns>
        public static ContentReference FirstProduct(this ContentReference variantContentReference)
        {
            var relations = Locate.Advanced.GetInstance<ILinksRepository>().GetRelationsByTarget<ProductVariation>(variantContentReference);
            return relations.Any() ? relations.FirstOrDefault().Source : null;
        }

        /// <summary>
        /// Get the content reference for a catalog key
        /// </summary>
        /// <param name="catalogKey"></param>
        /// <returns></returns>
        public static ContentReference GetContentReference(this CatalogKey catalogKey)
        {
            return Locate.ReferenceConverter().GetContentLink(catalogKey.CatalogEntryCode);
        }

        /// <summary>
        /// Returns true if the entry is a variant
        /// </summary>
        /// <param name="catalogKey"></param>
        /// <returns></returns>
        public static Boolean IsVariant(this CatalogKey catalogKey)
        {
            // Load the entry
            var entry = Locate.CatalogSystem().GetCatalogEntryDto(catalogKey.CatalogEntryCode,
                new CatalogEntryResponseGroup(CatalogEntryResponseGroup.ResponseGroup.CatalogEntryInfo));

            if (entry == null)
            {
                throw new ArgumentException("The catalog key with code '" + catalogKey.CatalogEntryCode + "' could not be translated into a Entry object");
            }

            return entry.IsVariant();
        }

        /// <summary>
        /// Returns true if the entry is a variant
        /// </summary>
        /// <param name="catalogContentReference"></param>
        /// <returns></returns>
        public static Boolean IsVariant(this ContentReference catalogContentReference)
        {
            // First do a quick check if it's a node or catalog.
            var contentType = Locate.ReferenceConverter().GetContentType(catalogContentReference);
            if (contentType != CatalogContentType.CatalogEntry) return false;
            
            // Get the catalog entry ID and load the entry
            var entryId = Locate.ReferenceConverter().GetObjectId(catalogContentReference);
            var entry = Locate.CatalogSystem().GetCatalogEntryDto(entryId,
                new CatalogEntryResponseGroup(CatalogEntryResponseGroup.ResponseGroup.CatalogEntryInfo));

            if (entry == null)
            {
                throw new ArgumentException("The content reference with ID '" + catalogContentReference.ID + "' could not be translated into a Entry object");
            }

            return entry.IsVariant();
        }

        /// <summary>
        /// Returns true if the entry is a variant
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public static Boolean IsVariant(this CatalogEntryDto entry)
        {
            return entry.CatalogEntry != null && entry.CatalogEntry.Count == 1 && entry.CatalogEntry[0].ClassTypeId == EntryType.Variation;
        }

        /// <summary>
        /// Gets the content reference from a set of price values.
        /// Assumes that the price values all belong to the same entry
        /// </summary>
        /// <param name="priceValues"></param>
        /// <returns></returns>
        public static ContentReference GetContentReference(this IEnumerable<IPriceDetailValue> priceValues)
        {
            var catalogKey = priceValues.First().CatalogKey;

            return catalogKey.GetContentReference();
        }
    }
}
