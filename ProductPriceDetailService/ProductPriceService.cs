﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Extensions;
using EPiServer.Events.ChangeNotification;
using EPiServer.ServiceLocation;
using Mediachase.Commerce;
using Mediachase.Commerce.Catalog;
using Mediachase.Commerce.Core;
using Mediachase.Commerce.Pricing;
using Mediachase.Commerce.Pricing.Database;
using EPiServer.Core;
using EPiServer.Commerce.Catalog.Linking;

namespace EPiServer.ExpertServices.ProductLevelPriceProvider
{
    public class ProductPriceService : IPriceService
    {
        private static readonly ServiceLocationHelper Locate = new ServiceLocationHelper(ServiceLocator.Current);
        private static readonly ICatalogSystem CatalogSystem = Locate.CatalogSystem();
        private static readonly PriceServiceDatabase PriceService = new PriceServiceDatabase(CatalogSystem, ServiceLocator.Current.GetInstance<IChangeNotificationManager>());
        private static readonly IContentLoader ContentService = Locate.ContentLoader();
        private static readonly ILinksRepository LinksRepository = ServiceLocator.Current.GetInstance<ILinksRepository>();

        /// <summary>
        /// Determines if the price will be stored on the product SKU or if it will be stored on the variants.
        /// If stored on the product (UseProductLevelPricing == true), the product price will be only stored on the product itself
        /// If stored on the variants (UseProductLevelPricing == false), the product price will be stored on all the product's variants
        /// </summary>
        public static Boolean UseProductLevelPricing
        {
            get
            {
                return !String.IsNullOrEmpty(Configuration.ProductLevelConfiguration.Value) && Convert.ToBoolean(Configuration.ProductLevelConfiguration.Value);
            }
        }


        #region IPriceService methods
        /// <summary>
        /// Gets a default price for a catalog entry. The default price for a market, currency, and catalog entry is the price available to all customers at a minimum quantity of 0.
        /// </summary>
        /// <param name="market"></param>
        /// <param name="validOn"></param>
        /// <param name="catalogKey"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public IPriceValue GetDefaultPrice(MarketId market, DateTime validOn, CatalogKey catalogKey, Currency currency)
        {
            return GetPrices(market, validOn, new CatalogKey[] { catalogKey }, new PriceFilter() { Currencies = new Currency[] { currency } }).FirstOrDefault();
        }

        /// <summary>
        /// Gets filtered pricing for a single catalog entry.
        /// </summary>
        /// <param name="market"></param>
        /// <param name="validOn"></param>
        /// <param name="catalogKey"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<IPriceValue> GetPrices(MarketId market, DateTime validOn, CatalogKey catalogKey, PriceFilter filter)
        {
            return this.GetPrices(market, validOn, new List<CatalogKey>() { catalogKey }, filter);
        }

        /// <summary>
        /// Gets filtered pricing for multiple catalog entries.
        /// </summary>
        /// <param name="market"></param>
        /// <param name="validOn"></param>
        /// <param name="catalogKeys"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<IPriceValue> GetPrices(MarketId market, DateTime validOn, IEnumerable<CatalogKey> catalogKeys, PriceFilter filter)
        {
            if (!UseProductLevelPricing)
            {
                return PriceService.GetPrices(market, validOn, catalogKeys, filter);
            }
            // First get all distinct products and keep the relationship between the original catalogkeys and the product only catalogkeys in a dictionary. 
            // Then fetch the prices for the products and create new IPriceValue for each catalogKey
            Dictionary<CatalogKey, CatalogKey> variantProductDictionary = catalogKeys.ToDictionary(catalogKey => catalogKey, ReplaceVariantCatalogKeyWithProduct);

            var productPrices = PriceService.GetPrices(market, validOn, variantProductDictionary.Values.Distinct(), filter);

            // Loop through each catalogKey, match it with a product catalog key to find the price for this particular catalog key
            return (
                from catalogKey in catalogKeys
                let productKey = variantProductDictionary.FirstOrDefault(x => x.Key.Equals(catalogKey)).Value
                let productPrice = productPrices.Where(p => p.CatalogKey.Equals(productKey))
                from priceValue in productPrice
                select new PriceValue(priceValue)
                {
                    CatalogKey = catalogKey
                }
            );
        }

        /// <summary>
        /// Gets filtered pricing for multiple catalog entries, in varying quantities.
        /// </summary>
        /// <param name="market"></param>
        /// <param name="validOn"></param>
        /// <param name="catalogKeysAndQuantities"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<IPriceValue> GetPrices(MarketId market, DateTime validOn, IEnumerable<CatalogKeyAndQuantity> catalogKeysAndQuantities, PriceFilter filter)
        {
            if (!UseProductLevelPricing)
            {
                return PriceService.GetPrices(market, validOn, catalogKeysAndQuantities, filter);
            }

            Dictionary<CatalogKeyAndQuantity, CatalogKeyAndQuantity> variantProductDictionary = catalogKeysAndQuantities.ToDictionary(catalogKey => catalogKey, ReplaceVariantCatalogKeyWithProduct);
            var productPrices = PriceService.GetPrices(market, validOn, variantProductDictionary.Values.Distinct(), filter);

            return (
                from catalogKeyAndQuantity in catalogKeysAndQuantities
                let productKey = variantProductDictionary.FirstOrDefault(x => x.Key.CatalogKey.Equals(catalogKeyAndQuantity.CatalogKey) && x.Key.Quantity == catalogKeyAndQuantity.Quantity).Value
                let productPrice = productPrices.Where(p => p.CatalogKey.Equals(productKey.CatalogKey))
                from priceValue in productPrice
                select new PriceValue(priceValue) { CatalogKey = catalogKeyAndQuantity.CatalogKey }
                );
        }

        /// <summary>
        /// Gets all price values for a single catalog entry.
        /// </summary>
        /// <param name="catalogKey"></param>
        /// <returns></returns>
        public IEnumerable<IPriceValue> GetCatalogEntryPrices(CatalogKey catalogKey)
        {
            return this.GetCatalogEntryPrices(new List<CatalogKey>(1) { catalogKey });
        }

        /// <summary>
        /// Gets all price values for one or more catalog entries.
        /// </summary>
        /// <param name="catalogKeys"></param>
        /// <returns></returns>
        public IEnumerable<IPriceValue> GetCatalogEntryPrices(IEnumerable<CatalogKey> catalogKeys)
        {
            if (!UseProductLevelPricing)
            {
                return PriceService.GetCatalogEntryPrices(catalogKeys);
            }

            // First get all distinct products and keep the relationship between the original variant catalogkeys and their parent product catalogkeys in a dictionary. 
            // Then fetch the prices for the products and create new IPriceValue for each variant catalogKey
            Dictionary<CatalogKey, CatalogKey> variantProductDictionary = catalogKeys.ToDictionary(catalogKey => catalogKey, ReplaceVariantCatalogKeyWithProduct);

            var productPrices = PriceService.GetCatalogEntryPrices(variantProductDictionary.Values.Distinct());

            var productVariantPrices = new List<IPriceValue>();
            foreach (var catalogKey in
                from catalogKey in catalogKeys
                let productKey = variantProductDictionary[catalogKey]
                let productPrice = productPrices.Where(p => Equals(p.CatalogKey, productKey))
                select catalogKey)
            {
                productVariantPrices.AddRange(
                    productPrices.Select
                    (
                        p => new PriceValue(p) { CatalogKey = catalogKey }
                    ));
            }
            return productVariantPrices;
        }

        /// <summary>
        /// Sets the price values for a catalog entry. To delete the price send in an empty enumeration of IPriceValues
        /// Used by Commerce Manager to set prices.
        /// ASSUMPTION! All prices belongs to one entry. Only special treatment for variants. Setting a price on a product will only set the price on the product even if you have ProductLevelPricing ON.
        /// </summary>
        /// <param name="catalogKey"></param>
        /// <param name="priceValues"></param>
        public void SetCatalogEntryPrices(CatalogKey catalogKey, IEnumerable<IPriceValue> priceValues)
        {
            if (IsReadOnly)
            {
                return;
            }

            // Find out if we're dealing with a variant or product.
            var reference = catalogKey.GetContentReference();
            bool isVariant = reference.IsVariant();

            // If we're dealing with a product set the price directly
            if (!isVariant)
            {
                PriceService.SetCatalogEntryPrices(catalogKey, priceValues);
            }

            // If we're dealing with a variant and we're not using product level pricing, add the prices to its siblings
            if (!UseProductLevelPricing && isVariant)
            {
                PriceService.SetCatalogEntryPrices(catalogKey, priceValues);

                // Find the siblings
                var siblings = GetSiblings(reference);

                // Set the price on each of the siblings
                foreach (var sibling in siblings)
                {
                    var siblingEntry = ContentService.Get<EntryContentBase>(sibling);
                    var siblingCatalogKey = new CatalogKey(catalogKey.ApplicationId, siblingEntry.Code);
                    PriceService.SetCatalogEntryPrices(siblingCatalogKey, priceValues.Select(p => new PriceValue(p){ CatalogKey = siblingCatalogKey }));
                }
            }
            // If we use product level pricing and the prices we want to to set belongs to a variant, find the parent product and set the values there instead.
            else if (UseProductLevelPricing && isVariant)
            { 
                // Find the parent
                var parent = reference.GetFirstProductParent();

                // Now set the price for the product
                var parentCatalogKey = new CatalogKey(catalogKey.ApplicationId, parent.Code);
                PriceService.SetCatalogEntryPrices(parentCatalogKey, priceValues.Select(p => new PriceValue(p) { CatalogKey = parentCatalogKey }));
            }
        }

        /// <summary>
        /// Sets the price values for multiple catalog entries.
        /// Any catalog entry listed in catalogKeys but not represented in priceValues will have its pricing data deleted.
        /// Not implemented!
        /// </summary>
        /// <param name="catalogKeys"></param>
        /// <param name="priceValues"></param>
        public void SetCatalogEntryPrices(IEnumerable<CatalogKey> catalogKeys, IEnumerable<IPriceValue> priceValues)
        {
            if (IsReadOnly)
            {
                return;
            }

            foreach (var catalogKey in catalogKeys)
            {
                SetCatalogEntryPrices(catalogKey, priceValues.Where(p => p.CatalogKey == catalogKey));
            }

        }

        /// <summary>
        /// Sets the price values for catalog entries to reflect changes in IPriceDetailService. This should ONLY be used by implementations of IPriceDetailService.
        /// If we use product level pricing and the prices belongs to variants (we assume it's either all variants or all products), find the siblings and create an identical set of prices for them.
        /// </summary>
        /// <param name="catalogKeys"></param>
        /// <param name="priceValues"></param>
        public void ReplicatePriceDetailChanges(IEnumerable<CatalogKey> catalogKeys, IEnumerable<IPriceValue> priceValues)
        {
            PriceService.ReplicatePriceDetailChanges(catalogKeys, priceValues);
        }

        /// <summary>
        /// Gets a value indicating whether or not the price service can modify prices.	
        /// This implementation is only read only to simplify logic. If you want to change prices please use the ProductPriceDetailService instead.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }
        #endregion

        #region Private Helper methods
        /// <summary>
        /// Fetches the sibling references for a variant
        /// Assumption: entryReference is a variant reference
        /// </summary>
        /// <param name="entryReference"></param>
        /// <returns></returns>
        private IEnumerable<ContentReference> GetSiblings(ContentReference entryReference)
        {
            var parent = entryReference.FirstProduct();

            var variantChildren = LinksRepository.GetRelationsBySource<ProductVariation>(parent);

            // Only return siblings to our entry reference
            return variantChildren.Where(v => !v.Target.CompareToIgnoreWorkID(entryReference)).Select(v => v.Target);
        }

        /// <summary>
        /// Replaces a variant's catalog key with its parent's
        /// </summary>
        /// <param name="catalogKeysAndQuantity"></param>
        /// <returns></returns>
        private CatalogKeyAndQuantity ReplaceVariantCatalogKeyWithProduct(CatalogKeyAndQuantity catalogKeysAndQuantity)
        {
            if (catalogKeysAndQuantity.CatalogKey.IsVariant())
            {
                var reference = catalogKeysAndQuantity.CatalogKey.GetContentReference();
                ProductContent productParent = reference.GetFirstProductParent();

                return productParent != null ? new CatalogKeyAndQuantity(new CatalogKey(catalogKeysAndQuantity.CatalogKey.ApplicationId, productParent.Code), catalogKeysAndQuantity.Quantity) : catalogKeysAndQuantity;
            }
            else
            {
                return catalogKeysAndQuantity;
            }
        }

        /// <summary>
        /// Replaces a variant's catalog key with its parent's
        /// </summary>
        /// <param name="catalogKey"></param>
        /// <returns></returns>
        private CatalogKey ReplaceVariantCatalogKeyWithProduct(CatalogKey catalogKey)
        {
            if (catalogKey.IsVariant())
            {
                var reference = catalogKey.GetContentReference();
                ProductContent productParent = reference.GetFirstProductParent();
                return productParent != null ? new CatalogKey(catalogKey.ApplicationId, productParent.Code) : catalogKey;
            }
            else
            {
                return catalogKey;
            }
        }
        #endregion
    }
}
