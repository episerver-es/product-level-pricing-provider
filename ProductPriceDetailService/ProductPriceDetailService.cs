﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Commerce.Catalog.Linking;
using EPiServer.Commerce.Extensions;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using log4net;
using Mediachase.Commerce;
using Mediachase.Commerce.Catalog;
using Mediachase.Commerce.Catalog.Managers;
using Mediachase.Commerce.Catalog.Objects;
using Mediachase.Commerce.Core;
using Mediachase.Commerce.Pricing;
using Mediachase.Commerce.Pricing.Database;
using ArgumentException = EPiServer.BaseLibrary.ArgumentException;
using EPiServer.Events.ChangeNotification;

namespace EPiServer.ExpertServices.ProductLevelPriceProvider
{
    public class ProductPriceDetailService : IPriceDetailService
    {
        private static readonly ServiceLocationHelper Locate = new ServiceLocationHelper(ServiceLocator.Current);
        private static readonly ICatalogSystem CatalogSystem = Locate.CatalogSystem();
        private static readonly PriceDetailDatabase PriceDetailService = new PriceDetailDatabase(Locate.ReferenceConverter());
        private static readonly PriceServiceDatabase PriceService = new PriceServiceDatabase(CatalogSystem, ServiceLocator.Current.GetInstance<IChangeNotificationManager>());
        private static readonly ILinksRepository LinksRepository = ServiceLocator.Current.GetInstance<ILinksRepository>();
        private static readonly ReferenceConverter ReferenceConverter = Locate.ReferenceConverter();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ProductPriceDetailService));

        /// <summary>
        /// Determines if the price will be stored on the product SKU or if it will be stored on the variants.
        /// If stored on the product (UseProductLevelPricing == true), the product price will be only stored on the product itself
        /// If stored on the variants (UseProductLevelPricing == false), the product price will be stored on all the product's variants
        /// </summary>
        public static Boolean UseProductLevelPricing
        {
            get
            {
                return !String.IsNullOrEmpty(Configuration.ProductLevelConfiguration.Value) && Convert.ToBoolean(Configuration.ProductLevelConfiguration.Value);
            }
        }


        #region IPriceDetailService methods
        /// <summary>
        /// Deletes the specified price values.	
        /// Assumes the price set belongs to one entry (product/variant).
        /// If a price gets deleted on an entry or product, we will threat the new price set as the master
        /// </summary>
        /// <param name="priceValueIds"></param>
        public void Delete(IEnumerable<long> priceValueIds)
        {
            // Get a reference before we delete it
            var priceValue = PriceDetailService.Get(priceValueIds.First());

            // Delete it
            PriceDetailService.Delete(priceValueIds);

            // If it's a variant and we're storing it on variant level, replace the sibling's prices with the new one.
            if (!UseProductLevelPricing && priceValue.CatalogKey.GetContentReference().IsVariant())
            {
                var newPriceValues = PriceDetailService.List(priceValue.CatalogKey.GetContentReference());
                SaveOnVariant(newPriceValues);
            }
        }

        /// <summary>
        /// Gets the price value identified by priceValueId.	
        /// </summary>
        /// <param name="priceValueId"></param>
        /// <returns></returns>
        public IPriceDetailValue Get(long priceValueId)
        {
            return PriceDetailService.Get(priceValueId);
        }

        /// <summary>
        /// Does this provider only read values
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Gets all price details for the catalogContentReference with paging support and filter for market, currencies and customer pricings.	
        /// </summary>
        /// <param name="catalogContentReference"></param>
        /// <param name="marketId"></param>
        /// <param name="filter"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public IList<IPriceDetailValue> List(ContentReference catalogContentReference, MarketId marketId, PriceFilter filter, int offset, int count, out int totalCount)
        {
            // We don't need to do anything if the catalogContentReference is not a variant or if it's a variant but we're storing the prices on the variant level (all variants have the same price).
            if (!UseProductLevelPricing || !catalogContentReference.IsVariant())
            {
                return PriceDetailService.List(catalogContentReference, marketId, filter, offset, count, out totalCount);
            }

            // The pased in content reference is a variant. Get the the product's prices instead.
            var parentEntry = catalogContentReference.GetFirstProductParent();
            // The default PriceService will list variants too so filter those out
            var productPrices = PriceDetailService.List(parentEntry.ContentLink, marketId, filter, offset, count, out totalCount).Where(p => p.CatalogKey.CatalogEntryCode.Equals(parentEntry.Code));

            // To mask that we're actually passing along the prices of the parent product instead of the variant the prices will have their Catalog Key switched.
            var variantCatalogKey = GetCatalogKeyFromReference(catalogContentReference);
            return productPrices.Select(p => new PriceDetailValue(p) { CatalogKey = variantCatalogKey })
                .Cast<IPriceDetailValue>()
                .ToList();
        }

        /// <summary>
        /// Gets all price details for the catalogContentReference with paging support.	
        /// </summary>
        /// <param name="catalogContentReference"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public IList<IPriceDetailValue> List(ContentReference catalogContentReference, int offset, int count, out int totalCount)
        {
            return this.List(catalogContentReference, MarketId.Empty, new PriceFilter(), offset, count, out totalCount);
        }

        /// <summary>
        /// Gets all price details for the catalogContentReference.	
        /// </summary>
        /// <param name="catalogContentReference"></param>
        /// <returns></returns>
        public IList<IPriceDetailValue> List(ContentReference catalogContentReference)
        {
            int totalCount;
            return this.List(catalogContentReference, MarketId.Empty, new PriceFilter(), new int(), new int(), out totalCount);
        }

        /// <summary>
        /// Sets the price detail values to reflect changes in IPriceService. This should ONLY be used by implementations of IPriceService.	
        /// </summary>
        /// <param name="catalogKeySet"></param>
        /// <param name="priceValuesList"></param>
        public void ReplicatePriceServiceChanges(IEnumerable<CatalogKey> catalogKeySet, IEnumerable<IPriceValue> priceValuesList)
        {
            PriceDetailService.ReplicatePriceServiceChanges(catalogKeySet, priceValuesList);
        }

        /// <summary>
        /// Updates or inserts the specified price values.	
        /// Stores the product price on the variation level
        /// </summary>
        /// <param name="priceValues"></param>
        /// <returns></returns>
        public IList<IPriceDetailValue> Save(IEnumerable<IPriceDetailValue> priceValues)
        {
            if (priceValues == null || !priceValues.Any()) return PriceDetailService.Save(priceValues);

            // Only give special treatment to variants
            // Assumption: All price values in the set belongs to either a variant or a product 
            if (!priceValues.First().CatalogKey.GetContentReference().IsVariant())
            {
                return PriceDetailService.Save(priceValues);
            }

            // Save on the product
            if (UseProductLevelPricing)
            {
                return SaveOnProduct(priceValues);
            }

            // Save on the variant (toggable via configuration)
            return SaveOnVariant(priceValues);
        }
        #endregion

        #region Private Helper methods
        /// <summary>
        /// Saves the price on the product
        /// </summary>
        /// <param name="priceValues"></param>
        /// <returns></returns>
        private IList<IPriceDetailValue> SaveOnProduct(IEnumerable<IPriceDetailValue> priceValues)
        {
            // Verify we're only dealing with prices that belongs to one variant and one parent product.
            if (!VerifyDistinctPriceValueSet(priceValues))
            {
                throw new ArgumentException("Multiple product parents found. The provider only supports a group of price values for a specific product or that all price values are variant prices that shares the same product parent.", "priceValues");
            }

            // Load the content to determine if we are dealing with a variation or not
            // Assumptions: 
            //  A variation is only bound to one product
            //  A group of price values all belong to one entry
            var contentReference = priceValues.GetContentReference();

            // If we don't find a content reference, let the default implementation handle it
            if (contentReference == null) return PriceDetailService.Save(priceValues);

            // Find our parent and save the price on it.
            var parentReference = contentReference.FirstProduct();
            var parent = GetCatalogEntryFromReference(parentReference);

            return PriceDetailService.Save(priceValues.Select(p => new PriceDetailValue(p)
            {
                CatalogKey = new CatalogKey(AppContext.Current.ApplicationId, parent.ID),
            }));

        }

        /// <summary>
        /// Saves the price on the product's variants.
        /// Assumption: price values is a collection of values from only one variant/product.
        /// </summary>
        /// <param name="priceValues"></param>
        /// <returns></returns>
        private IList<IPriceDetailValue> SaveOnVariant(IEnumerable<IPriceDetailValue> priceValues)
        {
            // First let the default price service handle it
            var savedPrices = PriceDetailService.Save(priceValues);

            // Get the parent
            // Assumption: A variation is only linked to one product
            var entryReference = priceValues.GetContentReference();
            var parent = entryReference.GetFirstProductParent();

            if (parent == null)
            {
                Logger.InfoFormat("Unable to save the price of: {0} to it's siblings. No parent found.", entryReference.ID);
                return savedPrices;
            }

            // Get the new values so we can save them on the siblings
            // This to keep prices in sync between the parent product's variants.
            var newPrices = PriceDetailService.List(entryReference);

            // Use the parent to find all the other variants
            IEnumerable<ContentReference> siblings = GetSiblings(entryReference);
            foreach (var sibling in siblings)
            {
                try
                {
                    var siblingCatalogKey = GetCatalogKeyFromReference(sibling);

                    // Now take the price values and change the id. Use SetCatalogEntryPrices as that will delete any older values automatically for us.
                    PriceService.SetCatalogEntryPrices(siblingCatalogKey, newPrices.Select(p => new PriceDetailValue(p)
                        {
                            PriceValueId = new long(),
                            CatalogKey = siblingCatalogKey
                        }));
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("Couldn't save price of: {0}", sibling.ID), ex);
                    throw;
                }
            }
            return savedPrices;
        }


        /// <summary>
        /// Checks a set of price values that they belong to a single parent (otherwise this provider doesn't work)
        /// </summary>
        /// <param name="priceValues"></param>
        /// <returns></returns>
        private bool VerifyDistinctPriceValueSet(IEnumerable<IPriceDetailValue> priceValues)
        {
            var firstParent = priceValues.First().CatalogKey.GetContentReference().GetFirstProductParent();
            return priceValues.All(p => p.CatalogKey.GetContentReference().GetFirstProductParent().Equals(firstParent));
        }

        /// <summary>
        /// Fetches the sibling references for a variant
        /// Assumption: entryReference is a variant reference
        /// </summary>
        /// <param name="entryReference"></param>
        /// <returns></returns>
        private IEnumerable<ContentReference> GetSiblings(ContentReference entryReference)
        {
            var parent = entryReference.FirstProduct();

            var variantChildren = LinksRepository.GetRelationsBySource<ProductVariation>(parent);

            // Only return siblings to our entry reference
            return variantChildren.Where(v => !v.Target.CompareToIgnoreWorkID(entryReference)).Select(v => v.Target);
        }

        /// <summary>
        /// Gets the catalog key from a content reference
        /// </summary>
        /// <param name="catalogContentReference"></param>
        /// <returns></returns>
        private CatalogKey GetCatalogKeyFromReference(ContentReference catalogContentReference)
        {
            Entry entry = GetCatalogEntryFromReference(catalogContentReference);
            return entry != null ? new CatalogKey(entry) : null;
        }

        /// <summary>
        /// Gets the entry associated with the content reference
        /// Use the old API to make sure we're not trying to read the same content twice in the same thread as that will cause a deadlock.
        /// </summary>
        /// <param name="catalogContentReference"></param>
        /// <returns></returns>
        private Entry GetCatalogEntryFromReference(ContentReference catalogContentReference)
        {
            if (catalogContentReference == null) return null;

            var entryId = ReferenceConverter.GetObjectId(catalogContentReference);

            return CatalogSystem.GetCatalogEntry(entryId,
                new CatalogEntryResponseGroup(CatalogEntryResponseGroup.ResponseGroup.CatalogEntryInfo));
        }
        #endregion
    }
}
