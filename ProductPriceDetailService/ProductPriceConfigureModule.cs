﻿using EPiServer.Framework;
using EPiServer.ServiceLocation;
using Mediachase.Commerce.Pricing;

namespace EPiServer.ExpertServices.ProductLevelPriceProvider
{
    [ModuleDependency(typeof(EPiServer.Commerce.Initialization.InitializationModule))]
    public class ProductPriceConfigureModule : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            if (Configuration.ProductLevelConfiguration.Value != null)
            {
                context.Container.Configure(c => c.For<IPriceDetailService>().Use<global::EPiServer.ExpertServices.ProductLevelPriceProvider.ProductPriceDetailService>());
                context.Container.Configure(c => c.For<IPriceService>().Use<global::EPiServer.ExpertServices.ProductLevelPriceProvider.ProductPriceService>());
            }
        }

        public void Initialize(EPiServer.Framework.Initialization.InitializationEngine context)
        {
        }

        public void Preload(string[] parameters)
        {
        }

        public void Uninitialize(EPiServer.Framework.Initialization.InitializationEngine context)
        {
        }
    }
}